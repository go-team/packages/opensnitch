#!/bin/sh
set -e 

# for some reason, go.exec.LookPath() fails to obtain the path of iptables
# on the ci environment, even if $PATH is set correctly.
echo "[+] PATH: $PATH"

log="/var/log/opensnitchd.log"

if [ -f /proc/modules ]; then
    echo "[+] loaded modules:"
    cat /proc/modules
fi

if [ -f $log ]; then
    echo "[+] opensnitchd log:"
    cat $log
fi
if grep "iptables not available" $log >/dev/null; then
    echo "[!] iptables not available, falling back to nftables"
    nft list ruleset | grep "ct state related,new queue flags bypass to 0"
    echo "[+] Interception rule (nftables): OK"
else
    /usr/sbin/iptables -t mangle -L OUTPUT
    /usr/sbin/iptables -t mangle -L OUTPUT | grep "NFQUEUE.*ctstate NEW,RELATED.*NFQUEUE num.*bypass"
    echo "[+] Interception rule (iptables): OK"
fi
